import React from 'react';
import {
    ActivityIndicator,
    FlatList,
    Keyboard,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from 'react-native';
// @ts-ignore
import Modal from 'react-native-modal';
import ModalBaseScene from './utils/ModalBaseScene';

class StationPicker extends ModalBaseScene {
    constructor(props) {
        super(props);
        this.state = {
            station: '',
            result: [],
            isLoading: false,
            selectedStation: '',
            containResult: 0,
        };
    }

    searchStation = (name) => {
        fetch('http://transport.opendata.ch/v1/locations?type=stations&fields[]=stations/name&fields[]=stations/id&fields[]=stations/icon&query=' + name)
            .then((response) => response.json())
            .then(json => {
                this.setState({ result: json.stations.filter(x => x.icon === 'train') });
            })
            .catch(error => {
                alert(error);
                console.log(error);
            }).finally(() => {
            this.setState({ isLoading: false });
        });
    };

    selectStation = (name) => {
        this.props.setStation(name);
    }

    renderModal(): React.ReactElement<any> {
        return (
            <Modal
                testID = {'modal'}
                isVisible = {this.isVisible()}
                onSwipeComplete = {this.close}
                swipeDirection = {['down']}
                style = {styles.view}>
                <TouchableWithoutFeedback onPress = {() => Keyboard.dismiss()}>
                    <View style = {[styles.content, { flex: this.state.containResult }]}>
                        <Text style = {styles.contentTitle}>Choose station</Text>
                        <TextInput
                            style = {styles.input}
                            onChangeText = {text => this.setState({
                                station: text,
                                isLoading: true,
                            }, () => {
                                this.searchStation(text);
                                this.setState({ containResult: this.state.station === '' ? 0 : 1 });

                            })}
                            placeholder = 'e.g. Lugano' />
                        <View style = {styles.listWrapper}>
                            {this.state.isLoading ? <ActivityIndicator /> :
                                <FlatList
                                    data = {this.state.result}
                                    keyExtractor = {item => item.id}
                                    renderItem = {({ item }) => (
                                        <TouchableOpacity
                                            activeOpacity = {.5}
                                            onPress = {() => {
                                                this.selectStation(item.name)
                                                this.setState({
                                                selectedStation: item.name,
                                                result: [],
                                                containResult: 0
                                            },
                                                this.close)}}>
                                            <Text style = {styles.itemList}>{item.name}</Text>
                                        </TouchableOpacity>
                                    )}
                                    style = {styles.resultList} />
                            }
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        justifyContent: 'flex-end',
        margin: 0,
    },
    content: {
        flexWrap: 'wrap',
        flexDirection: 'column',
        backgroundColor: 'white',
        padding: 22,
        maxHeight: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12,
    },
    input: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#F0F0F0',
        paddingVertical: 1,
        paddingHorizontal: 5,
        marginBottom: 5,
    },
    listWrapper: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        width: '100%',
    },
    resultList: {
        flex: 1,
    },
    itemList: {
        flex: 1,
        padding: 10,
        borderBottomWidth: 1,
        borderColor: '#F0F0F0'
    },
});

export default StationPicker;
