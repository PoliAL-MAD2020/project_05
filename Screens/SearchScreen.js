import React, { useState } from 'react';
import { Alert, StyleSheet, Text, TouchableNativeFeedback, View } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

import { CustomButton } from '../CustomButton';
import StationPicker from '../StationPicker';

function SearchScreen(props) {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [stationD, setStationD] = useState('Departure');
    const [stationA, setStationA] = useState('Arrival');
    const [selectedD, setSelectedD] = useState(false);
    const [selectedA, setSelectedA] = useState(false);


    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };
    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };
    const showDatepicker = () => {
        showMode('date');
    };
    const showTimepicker = () => {
        showMode('time');
    };

    const selectStationD = (name) => {
        setStationD(name);
        setSelectedD(true);
    };
    const selectStationA = (name) => {
        setStationA(name);
        setSelectedA(true);
    };

    const createAlert = (string) => {
        Alert.alert(
            string,
            "Please select a valid stations",
            [
                { text: "OK" }
            ],
            { cancelable: false }
        );
    };
    const navigateSearch = () => {
        if(stationD === 'Departure') {
            createAlert("No departure station selected!")
        }
        else if (stationA === 'Arrival'){
            createAlert("No arrival station selected!")
        }
        else if (stationD === stationA) {
            createAlert("Same departure and arrival stations!")
        }
        else {
            let hour = (date.getHours() + 2);
            let day = date.getDate();

            if(hour >= 24) {
                hour = (hour % 24);
                day = (day + 1);
            }

            const dateFormat = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + day;
            const hourFormat = hour + ':' + date.getMinutes();

            props.navigation.navigate('Results', {
                departure: {stationD},
                arrival: {stationA},
                date: {dateFormat},
                hour: {hourFormat},
            });
        }
    };

    return (
        <View style = {styles.container}>

            <View style = {styles.headerContainer}>
                <Text style = {styles.header}>Choose your trip</Text>
            </View>

            <View style = {styles.searchContainer}>

                <View style = {styles.pickerViewDeparture}>
                    <View style = {styles.selectedDeparture}>
                        {!selectedD ?
                            <Text numberOfLines={1} ellipsizeMode='tail'
                                style = {styles.text} >{stationD}</Text>
                        :
                            <Text numberOfLines={1} ellipsizeMode='tail'
                                  style = {styles.textS} >{stationD}</Text>
                        }
                    </View>
                    <View style = {styles.stationPickerBtn}>
                        <StationPicker setStation = {selectStationD} />
                    </View>
                </View>

                <View style = {styles.pickerViewArrival}>
                    <View style = {styles.selectedArrival}>
                        {!selectedA ?
                            <Text numberOfLines={1} ellipsizeMode='tail'
                                  style = {styles.text} > {stationA} </Text>
                            :
                            <Text numberOfLines={1} ellipsizeMode='tail'
                                  style = {styles.textS} > {stationA} </Text>
                        }
                    </View>
                    <View style = {styles.stationPickerBtn}>
                        <StationPicker setStation = {selectStationA} />
                    </View>
                </View>

                <View style = {styles.datePickerContainer}>
                    <TouchableNativeFeedback onPress = {showDatepicker}>
                        <View style = {styles.wrapperPickerArea}>
                            <Text style = {styles.hourAndDate}>{date.toLocaleDateString()}</Text>
                        </View>
                    </TouchableNativeFeedback>
                    <TouchableNativeFeedback onPress = {showTimepicker}>
                        <View style = {styles.wrapperPickerArea}>
                            <Text style = {styles.hourAndDate}>{date.toLocaleTimeString().substring(0, 5)}</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
                {show && (
                    <DateTimePicker
                        testID = "dateTimePicker"
                        timeZoneOffsetInMinutes = {0}
                        value = {date}
                        mode = {mode}
                        is24Hour = {true}
                        display = "default"
                        onChange = {onChange}
                        minimumDate = {new Date()}
                    />
                )}

                <View style = {styles.submit}>
                    <CustomButton
                        onPress = {() => navigateSearch()}
                        title = "Search" />
                </View>

            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
        padding: 15,
    },
    headerContainer: {
        alignSelf: 'flex-start',
        marginBottom: 10,
    },
    header: {
        fontSize: 20,
        color: 'gray',
        textAlign: 'center',
    },
    searchContainer: {
        height: 270,
        elevation: 5,
        padding: 8,
        borderRadius: 5,
        backgroundColor: 'white',
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        top: 30,
    },
    pickerViewDeparture: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 5,
        alignItems: 'center',
    },
    pickerViewArrival: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5,
    },


    selectedDeparture: {
        flex: 3,
    },
    selectedArrival: {
        flex: 3,
    },
    text: {
        color: 'grey',
        fontSize: 18,
    },
    textS: {
        color: 'black',
        fontSize: 18,
    },
    stationPickerBtn: {
        flex: 2,
    },


    datePickerContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    wrapperPickerArea: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    hourAndDate: {
        borderBottomWidth: .9,
        borderColor: 'gray',
        fontSize: 20,
        fontWeight: 'bold',
    },


    submit: {
        flex: .8,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});

export default SearchScreen;
