import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';

function refactorHour(x) {
    let dt = x * 1000;
    let date = new Date(dt);

    if (x == null) {
        return ('Schedule not available');
    }
    return (
        date.toLocaleTimeString().substring(0, 5)
    );
}

function TrainDetail(props) {
    let x = props.data.map(function (passitem, index) {
        return (
            <ListItem
                key = {passitem.station.name}
                title = {passitem.station.name}
                subtitle = {passitem.departureTimestamp == null ? refactorHour(passitem.arrivalTimestamp) : refactorHour(passitem.departureTimestamp)}
                bottomDivider
            />
        );
    });
    return (
        <View
            style = {{
                borderWidth: .8,
                borderBottomWidth: 2,
                borderColor: '#F0F0F0',
                borderBottomRightRadius: 10,
                borderBottomLeftRadius: 10,
                overflow: 'hidden',
            }}>
            {x}
        </View>
    );
}

export default class Directions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.route.params.data,
        };
    }

    render() {
        return (
            <View style = {styles.screen}>
                <FlatList
                    style = {styles.flatList}
                    data = {this.state.data}
                    keyExtractor = {item => item.departure.departure}
                    renderItem = {({ item }) =>
                        <View style = {styles.listContainer}>
                            <View style = {styles.headerList}>
                                <Text style = {styles.headerJourney}>{item.departure.station.name + ' → ' + item.arrival.station.name}</Text>
                                <Text style = {styles.headerTrainName}>{item.journey.name}</Text>
                            </View>
                            <TrainDetail data = {item.journey.passList}> </TrainDetail>
                        </View>
                    }
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    flatList: {
        flex: 1,
        padding: 15,
        paddingBottom: 0,
        marginBottom: 10,
    },
    listContainer: {
        borderRadius: 5,
        marginBottom: 10,
        overflow: 'hidden',
    },
    headerList: {
        flexDirection: 'row',
        backgroundColor: '#eaeaea',
        padding: 10,
    },
    headerJourney: {
        flex: 3,
        fontSize: 17,
    },
    headerTrainName: {
        flex: 1,
        fontSize: 17,
    },
    text: {
        fontSize: 30,
        textAlign: 'center',
        color: 'white',
    },
    item_list: {
        fontSize: 12,
        color: 'white',
        margin: 3,
    },
});
