import React from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export function DateAndTime(props) {
    let s = props.stations;
    let dt = props.time * 1000;
    let date = new Date(dt);

    if (props.time == null) {
        return (<Text>{s} Schedule unavailable</Text>);
    }
    return (
        <View style = {styles.station}>
            <Text style = {[styles.text, { fontWeight: 'bold' }]}>{date.toLocaleTimeString().substring(0, 5)}</Text>
            <Text style = {styles.text}>{'  '}{s}</Text>
        </View>
    );
}

const getLongDate = (timestamp) => {
    let dayOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    let dt = timestamp * 1000;
    let date = new Date(dt);
    return dayOfWeek[date.getDay()] + ', ' + date.getDate() + ' ' + month[date.getMonth()];
};

const areDateEqual = (t1, t2) => {
    let date1 = new Date(t1 * 1000);
    let date2 = new Date(t2 * 1000);
    return date1.toDateString() === date2.toDateString();
};

export default class Directions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            source: {},
            destination: {},
            departure: props.route.params.departure,
            arrival: props.route.params.arrival,
            day: props.route.params.date,
            hour: props.route.params.hour,
            lastDay: '',
        };
    }

    componentDidMount() {
        let str = 'http://transport.opendata.ch/v1/connections?from=' + this.state.departure.stationD +
            '&to=' + this.state.arrival.stationA +
            '&date=' + this.state.day.dateFormat +
            '&time=' + this.state.hour.hourFormat;
        console.log(str);
        return fetch(str)
            .then((response) => response.json())
            .then(json => {
                this.setState({
                    data: json.connections,
                    source: json.from,
                    destination: json.to,
                });
            }).catch(error => {
                alert(error);
                console.log(error);
            }).finally(() => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        const { isLoading, data } = this.state;
        let lastDay = '';

        const DateHeader = ({ date }) => {
            //console.log(date + "-----" + lastDay)
            if (!areDateEqual(date, lastDay)) {
                lastDay = date;
                return (
                    <View style = {styles.headerContainer}>
                        <Text style = {styles.header}>{getLongDate(date)}</Text>
                    </View>
                );
            } else {
                lastDay = date;
                return <View />;
            }
        };

        return (
            <View style = {styles.screen}>
                {isLoading ? <ActivityIndicator style = {styles.progressBar} /> : (
                    <FlatList
                        data = {data}
                        style = {styles.item_list}
                        keyExtractor = {(item, index) => index.toString()}
                        renderItem = {({ item }) =>
                            <View style = {styles.container}>
                                <DateHeader date = {item.sections[0].departure.departureTimestamp} />
                                <TouchableOpacity
                                    activeOpacity = {.7}
                                    style = {styles.card}
                                    onPress = {() => this.props.navigation.navigate('Details', { data: item.sections })}>
                                    <View style = {styles.cardContent}>
                                        <DateAndTime
                                            stations = {item.from.station.name}
                                            time = {item.sections[0].departure.departureTimestamp} />
                                        <DateAndTime
                                            stations = {item.to.station.name}
                                            time = {item.sections[item.transfers].arrival.arrivalTimestamp} />
                                        <View style = {styles.lastRow}>
                                            <Text style = {{ flex: 2 }}>{item.duration.substring(3, 8)}</Text>
                                            <Text style = {{ flex: 1 }}>Changes: {item.transfers}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                    />
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    container: {
        padding: 15,
    },
    headerContainer: {
        alignSelf: 'flex-start',
        marginBottom: 10,
    },
    header: {
        fontSize: 18,
        color: 'gray',
    },
    text: {
        fontSize: 15,
    },
    item_list: {
        fontSize: 12,
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 8,
        borderWidth: .8,
        borderBottomWidth: 2,
        borderColor: '#F0F0F0',
    },
    progressBar: {
        paddingTop: 20,
    },
    cardContent: {},
    lastRow: {
        flex: 1,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: '#F0F0F0',
        padding: 5,
    },
    station: {
        flex: 1,
        padding: 5,
        marginBottom: 5,
        flexDirection: 'row',
    },
});
