import React from 'react';
import { StyleSheet, Text, TouchableNativeFeedback, View } from 'react-native';


export const CustomButton = ({ title, onPress, style, textStyle }) => {
    return (
        <View style = {styles.wrapper}>
            <TouchableNativeFeedback onPress = {onPress}>
                <View style = {[styles.button, style]}>
                    <Text style = {[styles.text, textStyle]}>{title}</Text>
                </View>
            </TouchableNativeFeedback>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: '#00000000',
        borderRadius: 5,
        overflow: 'hidden',
        elevation: 4,
    },
    button: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 6,
    },

    text: {
        fontSize: 13,
        textTransform: 'uppercase',
        color: 'black',
    },
});
